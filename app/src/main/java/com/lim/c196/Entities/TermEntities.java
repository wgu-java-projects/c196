package com.lim.c196.Entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * A class to create the Term object
 * @author Christopher Lim
 */
@Entity(tableName = "Term")
public class TermEntities {
    @PrimaryKey(autoGenerate = true)
    private int termId;
    private String termTitle;
    private String termStartDate;
    private String termEndDate;

    /**
     * This is the class constructor for the Term Entities
     * @param termId
     * @param termTitle
     * @param termStartDate
     * @param termEndDate
     */
    public TermEntities(int termId, String termTitle, String termStartDate, String termEndDate) {
        this.termId = termId;
        this.termTitle = termTitle;
        this.termStartDate = termStartDate;
        this.termEndDate = termEndDate;
    }

    /**
     * This method is for getting the termId
     * @return termId
     */
    public int getTermId() {
        return termId;
    }

    /**
     * This method is for setting the termId
     * @param termId
     */
    public void setTermId(int termId) {
        this.termId = termId;
    }

    /**
     * This method is for getting the termTitle
     * @return termTitle
     */
    public String getTermTitle() {
        return termTitle;
    }

    /**
     * This method is for setting the termTitle
     * @param termTitle
     */
    public void setTermTitle(String termTitle) {
        this.termTitle = termTitle;
    }

    /**
     * This method is for getting the termStartDate
     * @return termStartDate
     */
    public String getTermStartDate() {
        return termStartDate;
    }

    /**
     * This method is for setting the termStartDate
     * @param termStartDate
     */
    public void setTermStartDate(String termStartDate) {
        this.termStartDate = termStartDate;
    }

    /**
     * This method is for getting the termEndDate
     * @return termEndDate
     */
    public String getTermEndDate() {
        return termEndDate;
    }

    /**
     * This method is for setting the termEndDate
     * @param termEndDate
     */
    public void setTermEndDate(String termEndDate) {
        this.termEndDate = termEndDate;
    }

    /**
     * This method provides default syntax that will be used from the database
     * @return termTitle
     */
    @Override
    public String toString() {
        return termTitle;
    }
}
