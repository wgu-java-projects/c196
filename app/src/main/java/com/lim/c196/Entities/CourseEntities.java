package com.lim.c196.Entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * A class to create the Course object
 * @author Christopher Lim
 */
@Entity(tableName = "Course")
public class CourseEntities {
    @PrimaryKey(autoGenerate = true)
    private int courseId;
    private int termId;
    private String courseTitle;
    private String courseStartDate;
    private String courseEndDate;
    private String courseStatus;
    private String courseInstructorName;
    private String courseInstructorPhoneNumber;
    private String courseInstructorEmail;
    private String courseNotes;

    /**
     * This is the class constructor for the Course Entities
     * @param courseId
     * @param termId
     * @param courseTitle
     * @param courseStartDate
     * @param courseEndDate
     * @param courseStatus
     * @param courseInstructorName
     * @param courseInstructorPhoneNumber
     * @param courseInstructorEmail
     * @param courseNotes
     */
    public CourseEntities(int courseId, int termId, String courseTitle, String courseStartDate, String courseEndDate, String courseStatus, String courseInstructorName, String courseInstructorPhoneNumber, String courseInstructorEmail, String courseNotes) {
        this.courseId = courseId;
        this.termId = termId;
        this.courseTitle = courseTitle;
        this.courseStartDate = courseStartDate;
        this.courseEndDate = courseEndDate;
        this.courseStatus = courseStatus;
        this.courseInstructorName = courseInstructorName;
        this.courseInstructorPhoneNumber = courseInstructorPhoneNumber;
        this.courseInstructorEmail = courseInstructorEmail;
        this.courseNotes = courseNotes;
    }

    /**
     * This method is for getting the courseId
     * @return courseId
     */
    public int getCourseId() {
        return courseId;
    }

    /**
     * This method is for setting the courseId
     * @param courseId
     */
    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    /**
     * This method is for getting the termId
     * @return termId
     */
    public int getTermId() {
        return termId;
    }

    /**
     * This method is for setting the termId
     * @param termId
     */
    public void setTermId(int termId) {
        this.termId = termId;
    }

    /**
     * This method is for getting the courseTitle
     * @return courseTitle
     */
    public String getCourseTitle() {
        return courseTitle;
    }

    /**
     * This method is for setting the courseTitle
     * @param courseTitle
     */
    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle;
    }

    /**
     * This method is for getting the courseStartDate
     * @return courseStartDate
     */
    public String getCourseStartDate() {
        return courseStartDate;
    }

    /**
     * This method is for setting the courseStartDate
     * @param courseStartDate
     */
    public void setCourseStartDate(String courseStartDate) {
        this.courseStartDate = courseStartDate;
    }

    /**
     * This method is for getting the courseEndDate
     * @return courseEndDate
     */
    public String getCourseEndDate() {
        return courseEndDate;
    }

    /**
     * This method is for setting the courseEndDate
     * @param courseEndDate
     */
    public void setCourseEndDate(String courseEndDate) {
        this.courseEndDate = courseEndDate;
    }

    /**
     * This method is for getting the courseStatus
     * @return courseStatus
     */
    public String getCourseStatus() {
        return courseStatus;
    }

    /**
     * This method is for setting the courseStatus
     * @param courseStatus
     */
    public void setCourseStatus(String courseStatus) {
        this.courseStatus = courseStatus;
    }

    /**
     * This method is for getting the courseInstructorName
     * @return
     */
    public String getCourseInstructorName() {
        return courseInstructorName;
    }

    /**
     * This method is setting the courseInstructorName
     * @param courseInstructorName
     */
    public void setCourseInstructorName(String courseInstructorName) {
        this.courseInstructorName = courseInstructorName;
    }

    /**
     * This method is for getting the courseInstructorPhoneNumber
     * @return courseInstructorPhoneNumber
     */
    public String getCourseInstructorPhoneNumber() {
        return courseInstructorPhoneNumber;
    }

    /**
     * This method is for setting the courseInstructorPhoneNumber
     * @param courseInstructorPhoneNumber
     */
    public void setCourseInstructorPhoneNumber(String courseInstructorPhoneNumber) {
        this.courseInstructorPhoneNumber = courseInstructorPhoneNumber;
    }

    /**
     * This method is for getting the courseInstructorEmail
     * @return courseInstructorEmail
     */
    public String getCourseInstructorEmail() {
        return courseInstructorEmail;
    }

    /**
     * This method is for setting the courseInstructorEmail
     * @param courseInstructorEmail
     */
    public void setCourseInstructorEmail(String courseInstructorEmail) {
        this.courseInstructorEmail = courseInstructorEmail;
    }

    /**
     * This method is for getting the courseNotes
     * @return courseNotes
     */
    public String getCourseNotes() {
        return courseNotes;
    }

    /**
     * This method is for setting the courseNotes
     * @param courseNotes
     */
    public void setCourseNotes(String courseNotes) {
        this.courseNotes = courseNotes;
    }

    /**
     * This method provides default syntax that will be used from the database
     * @return courseTitle
     */
    @Override
    public String toString() {
        return courseTitle;
    }
}
