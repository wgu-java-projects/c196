package com.lim.c196.Entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * A class to create the Assessment object
 * @author Christopher Lim
 */

@Entity(tableName = "Assessment")
public class AssessmentEntities {
    @PrimaryKey(autoGenerate = true)
    private int assessmentId;
    private int courseId;
    private String assessmentTitle;
    private String assessmentStartDate;
    private String assessmentEndDate;
    private String assessmentType;
    private String assessmentContent;

    /**
     * This is the class constructor for the Assessment Entities
     * @param assessmentId
     * @param courseId
     * @param assessmentTitle
     * @param assessmentStartDate
     * @param assessmentEndDate
     * @param assessmentType
     * @param assessmentContent
     */
    public AssessmentEntities(int assessmentId, int courseId, String assessmentTitle, String assessmentStartDate, String assessmentEndDate, String assessmentType, String assessmentContent) {
        this.assessmentId = assessmentId;
        this.courseId = courseId;
        this.assessmentTitle = assessmentTitle;
        this.assessmentStartDate = assessmentStartDate;
        this.assessmentEndDate = assessmentEndDate;
        this.assessmentType = assessmentType;
        this.assessmentContent = assessmentContent;
    }

    /**
     * This method is for getting the assessmentId
     * @return assessmentId
     */
    public int getAssessmentId() {
        return assessmentId;
    }

    /**
     * This method is for setting the assessmentId
     * @param assessmentId
     */
    public void setAssessmentId(int assessmentId) {
        this.assessmentId = assessmentId;
    }

    /**
     * This method is for getting the courseId
     * @return courseId
     */
    public int getCourseId() {
        return courseId;
    }

    /**
     * This method is for setting the courseId
     * @param courseId
     */
    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    /**
     * This method is for getting the assessmentTitle
     * @return assessmentTitle
     */
    public String getAssessmentTitle() {
        return assessmentTitle;
    }

    /**
     * This method is for setting the assessmentTitle
     * @param assessmentTitle
     */
    public void setAssessmentTitle(String assessmentTitle) {
        this.assessmentTitle = assessmentTitle;
    }

    /**
     * This method is for getting the assessmentStartDate
     * @return assessmentStartDate
     */
    public String getAssessmentStartDate() {
        return assessmentStartDate;
    }

    /**
     * This method is for setting the assessmentStartDate
     * @param assessmentStartDate
     */
    public void setAssessmentStartDate(String assessmentStartDate) {
        this.assessmentStartDate = assessmentStartDate;
    }

    /**
     * This method is for getting the assessmentEndDate
     * @return assessmentEndDate
     */
    public String getAssessmentEndDate() {
        return assessmentEndDate;
    }

    /**
     * This method is for setting the assessmentEndDate
     * @param assessmentEndDate
     */
    public void setAssessmentEndDate(String assessmentEndDate) {
        this.assessmentEndDate = assessmentEndDate;
    }

    /**
     * This method is for getting the assessmentType
     * @return assessmentType
     */
    public String getAssessmentType() {
        return assessmentType;
    }

    /**
     * This method is for setting the assessmentType
     * @param assessmentType
     */
    public void setAssessmentType(String assessmentType) {
        this.assessmentType = assessmentType;
    }

    /**
     * This method is for getting the assessmentContent
     * @return assessmentContent
     */
    public String getAssessmentContent() {
        return assessmentContent;
    }

    /**
     * This method is for setting the assessmentContent
     * @param assessmentContent
     */
    public void setAssessmentContent(String assessmentContent) {
        this.assessmentContent = assessmentContent;
    }

    /**
     * This method provides default syntax that will be used from the database
     * @return assessmentTitle
     */
    @Override
    public String toString() {
        return assessmentTitle;
    }
}
