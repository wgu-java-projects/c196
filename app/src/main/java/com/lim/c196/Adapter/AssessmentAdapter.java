package com.lim.c196.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import com.lim.c196.Entities.AssessmentEntities;
import com.lim.c196.R;

/**
 * A class for Assessment Adapter
 * @author Christopher Lim
 */
public class AssessmentAdapter extends RecyclerView.Adapter<AssessmentAdapter.AssessmentViewHolder> {
    private OnItemClickListener onItemClickListener;
    private List<AssessmentEntities> mAssessment;
    private final Context context;
    private final LayoutInflater layoutInflater;

    /**
     * This is the AssessmentAdapter constructor
     * @param context
     * @param listener
     */
    public AssessmentAdapter(Context context, OnItemClickListener listener) {
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.onItemClickListener = listener;
    }

    /**
     * This method is a wrapper around a view that contains the layout for an individual item
     * in the list
     */
    public class AssessmentViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewAssessmentTitle;
        private TextView textViewAssessmentStartDate;
        private TextView textViewAssessmentEndDate;
        private TextView textViewAssessmentType;

        private AssessmentViewHolder(View itemView) {
            super(itemView);
            textViewAssessmentTitle = itemView.findViewById(R.id.assessmentListItemTitle);
            textViewAssessmentStartDate = itemView.findViewById(R.id.assessmentListItemStartdate);
            textViewAssessmentEndDate = itemView.findViewById(R.id.assessmentListItemEnddate);
            textViewAssessmentType = itemView.findViewById(R.id.assessmentListItemType);

            // this item view click is used for when clicking one of the assessment item from the list
            // by getting their position from the list
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        AssessmentEntities current = mAssessment.get(position);
                        onItemClickListener.onItemClick(current);
                    }
                }
            });

        }
    }

    /**
     * This is an interface definition for a callback to be invoke when
     * an item in this AdapterView has been clicked.
     */
    public interface OnItemClickListener {
        void onItemClick(AssessmentEntities assessment);
    }

    /**
     * This method is needed to create a new ViewHolder
     * @param parent The ViewGroup into which the new View will be added after it is bound to
     *               an adapter position.
     * @param viewType The view type of the new View.
     *
     * @return AssessmentAdapter
     */
    @NonNull
    @Override
    public AssessmentAdapter.AssessmentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // This is where we use the layoutInflater for the assessment item
        View itemView= layoutInflater.inflate(R.layout.activity_assessment_item,parent,false);
        return new AssessmentAdapter.AssessmentViewHolder(itemView);
    }

    /**
     * This method is to associate a ViewHolder with data.
     * @param holder The ViewHolder which should be updated to represent the contents of the
     *        item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(@NonNull AssessmentAdapter.AssessmentViewHolder holder, int position) {
        // if the assessment are not null we passes this information the assment items
        if(mAssessment!= null){
            AssessmentEntities current = mAssessment.get(position);
            String title = current.getAssessmentTitle();
            String start = current.getAssessmentStartDate();
            String end = current.getAssessmentEndDate();
            String type = current.getAssessmentType();

            holder.textViewAssessmentTitle.setText(title);
            holder.textViewAssessmentStartDate.setText(start);
            holder.textViewAssessmentEndDate.setText(end);
            holder.textViewAssessmentType.setText(type);
        }
        else{
            // if we cannot find the data for the assessment item we set the text to these
            holder.textViewAssessmentTitle.setText("No Assessment Title");
            holder.textViewAssessmentStartDate.setText("No Assessment Start Date");
            holder.textViewAssessmentEndDate.setText("No Assessment End Date");
            holder.textViewAssessmentType.setText("No Assessment Type");
        }
    }

    /**
     * This method is return the number of items in the collection you are adapting.
     * @return 0
     */
    @Override
    public int getItemCount() {
        if (mAssessment != null) {
            return mAssessment.size();
        } else {
            return 0;
        }
    }

    /**
     * This method is used every time the list is updated.
     * @param assessments
     */
    public void setAssessments(List<AssessmentEntities> assessments) {
        mAssessment = assessments;
        notifyDataSetChanged();
    }
}
