package com.lim.c196.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import com.lim.c196.Entities.TermEntities;
import com.lim.c196.R;

/**
 * A class for Term Adapter
 * @author Christopher Lim
 */
public class TermAdapter extends RecyclerView.Adapter<TermAdapter.TermViewHolder>{

    private OnItemClickListener onItemClickListener;
    private List<TermEntities> mTerms;
    private final Context context;
    private final LayoutInflater layoutInflater;

    /**
     * This is the constructor for TermAdapter
     * @param context
     * @param listener
     */
    public TermAdapter(Context context, OnItemClickListener listener) {
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.onItemClickListener = listener;
    }

    /**
     * This method is a wrapper around a view that contains the layout for an individual item
     * in the list
     */
    public class TermViewHolder extends RecyclerView.ViewHolder{
        private TextView textViewTermTitle;
        private TextView textViewTermStartDate;
        private TextView textViewTermEndDate;

        private TermViewHolder(View itemView) {
            super(itemView);
            textViewTermTitle = itemView.findViewById(R.id.termListItemTitle);
            textViewTermStartDate = itemView.findViewById(R.id.termListItemStartdate);
            textViewTermEndDate = itemView.findViewById(R.id.termListItemEnddate);

            // this item view click is used for when clicking one of the term item from the list
            // by getting their position from the list
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        TermEntities current = mTerms.get(position);
                        onItemClickListener.onItemClick(current);
                    }
                }
            });
        }
    }

    /**
     * This is an interface definition for a callback to be invoke when
     * an item in this AdapterView has been clicked.
     */
    public interface OnItemClickListener {
        void onItemClick(TermEntities term);
    }


    /**
     * This method is needed to create a new ViewHolder
     * @param parent The ViewGroup into which the new View will be added after it is bound to
     *               an adapter position.
     * @param viewType The view type of the new View.
     *
     * @return
     */
    @NonNull
    @Override
    public TermAdapter.TermViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // This is where we use the layoutInflater for the term item
        View itemView= layoutInflater.inflate(R.layout.activity_term_item,parent,false);
        return new TermViewHolder(itemView);
    }

    /**
     * This method is to associate a ViewHolder with data.
     * @param holder The ViewHolder which should be updated to represent the contents of the
     *        item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(@NonNull TermAdapter.TermViewHolder holder, int position) {
        // if the terms are not null we passes this information the term items
        if(mTerms!= null){
            TermEntities current = mTerms.get(position);
            String title = current.getTermTitle();
            String start = current.getTermStartDate();
            String end = current.getTermEndDate();

            holder.textViewTermTitle.setText(title);
            holder.textViewTermStartDate.setText(start);
            holder.textViewTermEndDate.setText(end);
        }
        else{
            // if we cannot find the data for the term item we set the text to these
            holder.textViewTermTitle.setText("No Term title");
            holder.textViewTermStartDate.setText("No Term Start Date");
            holder.textViewTermEndDate.setText("No Term End Date");
        }
    }

    /**
     * This method is return the number of items in the collection you are adapting.
     * @return 0
     */
    @Override
    public int getItemCount() {
        if (mTerms != null) {
            return mTerms.size();
        } else {
            return 0;
        }
    }

    /**
     * This method is used every time the list is updated.
     * @param terms
     */
    public void setTerms(List<TermEntities> terms) {
        mTerms = terms;
        notifyDataSetChanged();
    }
}
