package com.lim.c196.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import com.lim.c196.Entities.CourseEntities;
import com.lim.c196.R;

/**
 * A class for Course Adapter
 * @author Christopher Lim
 */
public class CourseAdapter extends RecyclerView.Adapter<CourseAdapter.CourseViewHolder>{
    private OnItemClickListener onItemClickListener;
    private List<CourseEntities> mCourses;
    private final Context context;
    private final LayoutInflater layoutInflater;


    /**
     * This is the CourseAdapter constructor
     * @param context
     * @param listener
     */
    public CourseAdapter(Context context, OnItemClickListener listener) {
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.onItemClickListener = listener;
    }

    /**
     * This method is a wrapper around a view that contains the layout for an individual item
     * in the list
     */
    public class CourseViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewCourseTitle;
        private TextView textViewCourseStartDate;
        private TextView textViewCourseEndDate;
        private TextView textViewCourseStatus;
        private TextView textViewCourseInstructorName;

        private CourseViewHolder(View itemView) {
            super(itemView);
            textViewCourseTitle = itemView.findViewById(R.id.courseListItemTitle);
            textViewCourseStartDate = itemView.findViewById(R.id.courseListItemStartdate);
            textViewCourseEndDate = itemView.findViewById(R.id.courseListItemEnddate);
            textViewCourseStatus = itemView.findViewById(R.id.courseListItemStatus);
            textViewCourseInstructorName = itemView.findViewById(R.id.courseListItemInstructorsName);

            // this item view click is used for when clicking one of the courses item from the list
            // by getting their position from the list
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        CourseEntities current = mCourses.get(position);
                        onItemClickListener.onItemClick(current);
                    }
                }
            });

        }
    }

    /**
     * This is an interface definition for a callback to be invoke when
     * an item in this AdapterView has been clicked.
     */
    public interface OnItemClickListener {
        void onItemClick(CourseEntities course);
    }

    /**
     * This method is needed to create a new ViewHolder
     * @param parent The ViewGroup into which the new View will be added after it is bound to
     *               an adapter position.
     * @param viewType The view type of the new View.
     *
     * @return CourseAdapter
     */
    @NonNull
    @Override
    public CourseAdapter.CourseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // This is where we use the layoutInflater for the course item
        View itemView= layoutInflater.inflate(R.layout.activity_course_item,parent,false);
        return new CourseAdapter.CourseViewHolder(itemView);
    }

    /**
     * This method is to associate a ViewHolder with data.
     * @param holder The ViewHolder which should be updated to represent the contents of the
     *        item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(@NonNull CourseAdapter.CourseViewHolder holder, int position) {
        // if the courses are not null we passes this information the course items
        if(mCourses!= null){
            CourseEntities current = mCourses.get(position);
            String title = current.getCourseTitle();
            String start = current.getCourseStartDate();
            String end = current.getCourseEndDate();
            String status = current.getCourseStatus();
            String instructorsName = current.getCourseInstructorName();


            holder.textViewCourseTitle.setText(title);
            holder.textViewCourseStartDate.setText(start);
            holder.textViewCourseEndDate.setText(end);
            holder.textViewCourseStatus.setText(status);
            holder.textViewCourseInstructorName.setText(instructorsName);
        }
        else{
            // if we cannot find the data for the course item we set the text to these
            holder.textViewCourseTitle.setText("No Course title");
            holder.textViewCourseStartDate.setText("No Course Start Date");
            holder.textViewCourseEndDate.setText("No Course End Date");
            holder.textViewCourseStatus.setText("No Course Status");
            holder.textViewCourseInstructorName.setText("No Course Instructors Name");
        }
    }

    /**
     * This method is return the number of items in the collection you are adapting.
     * @return 0
     */
    @Override
    public int getItemCount() {
        if (mCourses != null) {
            return mCourses.size();
        } else {
            return 0;
        }
    }

    /**
     * This method is used every time the list is updated.
     * @param courses
     */
    public void setCourses(List<CourseEntities> courses) {
        mCourses = courses;
        notifyDataSetChanged();
    }
}
