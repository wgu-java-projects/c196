package com.lim.c196.UI;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.lim.c196.Database.Repository;
import com.lim.c196.Entities.AssessmentEntities;
import com.lim.c196.Entities.CourseEntities;
import com.lim.c196.R;
import android.widget.AdapterView.OnItemSelectedListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * A class for Assessment Detail Activity
 * @author Christopher Lim
 */
public class AssessmentDetail extends AppCompatActivity implements OnItemSelectedListener {

    private EditText assessmentNameEditText;
    private EditText assessmentStartDateEditText;
    private EditText assessmentEndDateEditText;
    private Spinner assessmentType;
    private Spinner assessmentCourse;
    private EditText assessmentContentEditText;
    private Button saveButton;

    Repository repository;
    private int courseId;
    private int assessmentId;
    private String type;
    private List<CourseEntities> courses;
    AssessmentEntities currentAssessment;

    // This is used for the Calendar Date Picker
    DatePickerDialog.OnDateSetListener assessmentStartDate;
    final Calendar assessmentCalendarStartDate = Calendar.getInstance();

    DatePickerDialog.OnDateSetListener assessmentEndDate;
    final Calendar assessmentCalendarEndDate = Calendar.getInstance();

    /**
     * This is the constructor for Assessment Detail
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assessment_detail);

        repository = new Repository(getApplication());

        // this is for assessment edit text
        assessmentNameEditText = findViewById(R.id.assessmentNameEditText);
        assessmentStartDateEditText = findViewById(R.id.assessmentEditStartDate);
        assessmentEndDateEditText = findViewById(R.id.assessmentEditEndDate);
        assessmentContentEditText = findViewById(R.id.assessmentContentEditText);

        // this is for the spinner or dropdown
        assessmentCourse = findViewById(R.id.assessmentCourseSpinner);
        assessmentType = (Spinner) findViewById(R.id.assessmentTypeSpinner);

        // this is for the assessment save button
        saveButton = findViewById(R.id.saveButton3);

        // This is for the date picker
        String dateFormat = "MM/dd/yy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat, Locale.US);

        // This is for the start date
        assessmentStartDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date date = Calendar.getInstance().getTime();
                String info = assessmentStartDateEditText.getText().toString();

                if (info.equals("")){
                    info = simpleDateFormat.format(date);
                }

                try {
                    assessmentCalendarStartDate.setTime(simpleDateFormat.parse(info));
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                new DatePickerDialog(AssessmentDetail.this, assessmentStartDate, assessmentCalendarStartDate
                        .get(Calendar.YEAR), assessmentCalendarStartDate.get(Calendar.MONTH), assessmentCalendarStartDate.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        assessmentStartDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String dateFormat = "MM/dd/yy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);

                assessmentCalendarStartDate.set(Calendar.YEAR, year);
                assessmentCalendarStartDate.set(Calendar.MONTH, month);
                assessmentCalendarStartDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                assessmentStartDateEditText.setText(sdf.format(assessmentCalendarStartDate.getTime()));
            }
        };

        // This is for the end date
        assessmentEndDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date date = Calendar.getInstance().getTime();
                String info = assessmentEndDateEditText.getText().toString();

                if (info.equals("")){
                    info = simpleDateFormat.format(date);
                }

                try {
                    assessmentCalendarEndDate.setTime(simpleDateFormat.parse(info));
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                new DatePickerDialog(AssessmentDetail.this, assessmentEndDate, assessmentCalendarEndDate
                        .get(Calendar.YEAR), assessmentCalendarEndDate.get(Calendar.MONTH), assessmentCalendarEndDate.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        assessmentEndDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String dateFormat = "MM/dd/yy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);

                assessmentCalendarEndDate.set(Calendar.YEAR, year);
                assessmentCalendarEndDate.set(Calendar.MONTH, month);
                assessmentCalendarEndDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                assessmentEndDateEditText.setText(sdf.format(assessmentCalendarEndDate.getTime()));
            }
        };


        // this will pass the assessment data into the assessment adapter
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            assessmentId = extras.getInt("assessmentId");
            courseId = extras.getInt("courseId");

            String assessmentTitle = extras.getString("title");
            String assessmentStartDate = extras.getString("start");
            String assessmentEndDate = extras.getString("end");
            type = extras.getString("type");

            String assessmentContent = extras.getString("content");

            // this will fill the Edit Text with the Assessment Details
            assessmentNameEditText.setText(assessmentTitle);
            assessmentStartDateEditText.setText(assessmentStartDate);
            assessmentEndDateEditText.setText(assessmentEndDate);
            assessmentContentEditText.setText(assessmentContent);
        }

        // this populate the course spinner
        courses = repository.getAllCourses();

        // we are making an array list of courses
        ArrayList<CourseEntities> courseEntitiesArrayList = new ArrayList<>();
        courseEntitiesArrayList.addAll(courses);

        // This is where we add the course array list to the array adapter
        // and then add them to the drop down of the spinner
        ArrayAdapter<CourseEntities> spinnerCourseAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, courseEntitiesArrayList);
        spinnerCourseAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        assessmentCourse.setAdapter(spinnerCourseAdapter);

        // if the course id is greater than 0 we make the selection courseId -1
        if (courseId > 0) {
            assessmentCourse.setSelection(courseId -1);
        }

        // This is used when selecting the course list
        assessmentCourse.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CourseEntities selectedCourse = courses.get(position);
                courseId = selectedCourse.getCourseId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        // this populate the assessment type spinner
        ArrayAdapter<CharSequence> assessmentTypeAdapter = ArrayAdapter.createFromResource(
                this, R.array.assessment_type_list, android.R.layout.simple_spinner_item);
        assessmentTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        assessmentType.setAdapter(assessmentTypeAdapter);
        // if the type of the assessment is not null and equal to performance, we set the selection to zero
        // and if it is equal to objective. We set the selection to one
        if (type != null) {
            if (type.equals("Performance")) {
                assessmentType.setSelection(0);
            }
            else if (type.equals("Objective"))  {
                assessmentType.setSelection(1);
            }
        }

        // This is used when selecting the type of assessment
        assessmentType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                type = (String) parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        // this is for saving a new assessment
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String assessmentTitle = assessmentNameEditText.getText().toString();
                String assessmentStartDate = assessmentStartDateEditText.getText().toString();
                String assessmentEndDate = assessmentEndDateEditText.getText().toString();
                String assessmentContent = assessmentContentEditText.getText().toString();

                repository = new Repository(getApplication());
                AssessmentEntities assessment;

                // if the edit text fields are empty it will show the toast message
                if (assessmentTitle.trim().isEmpty() || assessmentStartDate.trim().isEmpty() || assessmentEndDate.trim().isEmpty()
                        || assessmentContent.trim().isEmpty()) {
                    Toast.makeText(AssessmentDetail.this, "The Fields are empty.", Toast.LENGTH_LONG).show();
                    return;
                }

                // if the assessmentId is not zero, we use the update method to update the assessment
                if (assessmentId != 0) {
                    // This is for existing assessment
                    assessment = new AssessmentEntities(assessmentId, courseId, assessmentTitle, assessmentStartDate, assessmentEndDate,
                            type, assessmentContent);
                    repository.update(assessment);
                    Toast.makeText(AssessmentDetail.this, assessmentTitle + " has been successfully updated.", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(AssessmentDetail.this, AssessmentList.class);
                    startActivity(intent);
                } else {
                    // This is for making a new assessment
                    assessment = new AssessmentEntities(assessmentId, courseId, assessmentTitle, assessmentStartDate, assessmentEndDate,
                            type, assessmentContent);
                    repository.insert(assessment);
                    Toast.makeText(AssessmentDetail.this, assessmentTitle + " has been successfully added.", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(AssessmentDetail.this, AssessmentList.class);
                    startActivity(intent);
                }
            }
        });
    }

    // This method is needed for OnItemSelectedListener
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
    }

    // This method is needed for OnItemSelectedListener
    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    /**
     * This will create the menu options for Assessment detail page
     * @param menu The options menu in which you place your items.
     *
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_assessment_detail, menu);
        return true;
    }

    /**
     * This method is used to select the menu items.
     * @param item The menu item that was selected.
     *
     * @return true
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        repository = new Repository(getApplication());
        String assessmentTitle = assessmentNameEditText.getText().toString();
        String assessmentStartDate = assessmentStartDateEditText.getText().toString();
        String assessmentEndDate = assessmentEndDateEditText.getText().toString();
        String assessmentContent = assessmentContentEditText.getText().toString();

        String dateFormat = "MM/dd/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat, Locale.US);

        // This menu item let us return to the course list
        if (item.getItemId() == R.id.assessment_back) {
            Intent intent = new Intent(AssessmentDetail.this, AssessmentList.class);
            startActivity(intent);
        }

        // This menu item let us delete the assessment
        if (item.getItemId() == R.id.assessment_delete) {
            if (assessmentTitle.trim().isEmpty() || assessmentStartDate.trim().isEmpty() || assessmentEndDate.trim().isEmpty()
                    || assessmentContent.trim().isEmpty()) {
                Toast.makeText(AssessmentDetail.this, "The Fields are empty.", Toast.LENGTH_LONG).show();
                return true;
            } else {

                for (AssessmentEntities assessment : repository.getAllAssessments()) {
                    if (assessment.getAssessmentId() == assessmentId) {
                        currentAssessment = assessment;
                    } else if (assessment.getAssessmentId() == 0) {
                        Toast.makeText(AssessmentDetail.this, "Cannot Delete an Assessment that has no assessmentId.", Toast.LENGTH_LONG).show();
                        return true;
                    }
                }

                if (assessmentId != 0) {
                    repository.delete(currentAssessment);
                    Toast.makeText(AssessmentDetail.this, currentAssessment.getAssessmentTitle() + " has been successfully deleted.", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(AssessmentDetail.this, AssessmentList.class);
                    startActivity(intent);
                }
            }
        }


        // This menu will send alerts for the course start date
        if (item.getItemId() == R.id.action_notify_start_assessment) {
            try {
                Date startDate = simpleDateFormat.parse(assessmentStartDate);

                Long startDateTrigger = startDate.getTime();
                Intent startDateIntent = new Intent(AssessmentDetail.this, MyReceiver.class);
                startDateIntent.putExtra("key", assessmentTitle + " has started!");

                PendingIntent sender = PendingIntent.getBroadcast(AssessmentDetail.this, Home.numAlert++, startDateIntent,
                        PendingIntent.FLAG_IMMUTABLE);

                AlarmManager startDateAlarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                startDateAlarmManager.set(AlarmManager.RTC_WAKEUP, startDateTrigger, sender);

                return true;
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }

        // This menu will send alerts for the course end date
        if (item.getItemId() == R.id.action_notify_end_assessment) {
            try {
                Date endDate = simpleDateFormat.parse(assessmentEndDate);

                Long endDateTrigger = endDate.getTime();
                Intent endDateIntent = new Intent(AssessmentDetail.this, MyReceiver.class);
                endDateIntent.putExtra("key", assessmentTitle + " has ended!");

                PendingIntent sender = PendingIntent.getBroadcast(AssessmentDetail.this, Home.numAlert++, endDateIntent,
                        PendingIntent.FLAG_IMMUTABLE);

                AlarmManager endDateAlarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                endDateAlarmManager.set(AlarmManager.RTC_WAKEUP, endDateTrigger, sender);

                return true;
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }

        return true;
    }
}