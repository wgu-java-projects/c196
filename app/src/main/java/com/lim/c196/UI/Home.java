package com.lim.c196.UI;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.lim.c196.Database.Repository;
import com.lim.c196.Entities.AssessmentEntities;
import com.lim.c196.Entities.CourseEntities;
import com.lim.c196.Entities.TermEntities;
import com.lim.c196.R;

/**
 * A class for Main Activity
 * @author Christopher Lim
 */
public class Home extends AppCompatActivity {
    public static int numAlert;
    private Repository repository;

    /**
     * This method creates the MainActivity page
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        repository = new Repository(getApplication());
    }

    /**
     * This method is used to generate terms, courses and assessment.
     * And then it navigates to the TermList page
     * @param view
     */
    public void ViewAllTerms(View view) {
        try {
            // This insert the term 1
            TermEntities term = new TermEntities(1, "Term 1", "1/1/2024", "5/31/2024");
            repository.insert(term);
            // This insert the C196 Course
            CourseEntities course = new CourseEntities(1, 1, "Mobile Application Development - C196", "1/1/2024", "1/31/2024", "In Progress", "John Doe", "(555) 555-5555", "john.doe@sample.com", "This course is the course before the last.");
            repository.insert(course);
            // This insert the assessment for C196 Course
            AssessmentEntities assessment = new AssessmentEntities(1, 1, "C196 Assessment", "1/7/2024","1/31/2024","Performance","This Performance Assessment will ask you to make a Term Tracker App");
            repository.insert(assessment);

            // This insert the C868 Course
            CourseEntities course2 = new CourseEntities(2, 1, "Software Development Capstone - C868", "2/1/2024", "2/29/2024", "Plan to Take", "John Doe", "(555) 555-5555", "john.doe@sample.com", "This course will be the last course needed to Graduate from WGUs Software Developers Bachelors Program.");
            repository.insert(course2);
            // This insert the assessment for C868 Course
            AssessmentEntities assessment2 = new AssessmentEntities(2, 2, "Capstone Assessment", "2/1/2024","2/29/2024","Performance","This Performance Assessment for C868.");
            repository.insert(assessment2);

            // This insert the term 2
            TermEntities term2 = new TermEntities(2, "Term 2", "6/1/2024", "12/31/2024");
            repository.insert(term2);

            // This insert the term 3
            TermEntities term3 = new TermEntities(3, "Term 3", "1/1/2025", "5/31/2025");
            repository.insert(term3);

            // This insert the Fake Course
            CourseEntities course3 = new CourseEntities(3, 3, "Fake Course", "1/1/2025", "2/29/2025", "Plan to Take", "John Doe", "(555) 555-5555", "john.doe@sample.com", "This course is faker.");
            repository.insert(course3);
            // This insert the assessment for Fake Course
            AssessmentEntities assessment3 = new AssessmentEntities(3, 3, "Faker Course - Assessment", "1/1/2025","1/29/2025","Objective","This Objective Assessment for Fake Course.");
            repository.insert(assessment3);

            Intent intent = new Intent(Home.this, TermList.class);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}