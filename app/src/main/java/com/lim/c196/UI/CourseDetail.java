package com.lim.c196.UI;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.lim.c196.Database.Repository;
import com.lim.c196.Entities.AssessmentEntities;
import com.lim.c196.Entities.CourseEntities;
import com.lim.c196.Entities.TermEntities;
import com.lim.c196.R;
import android.widget.AdapterView.OnItemSelectedListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * A class for Course Detail Activity
 * @author Christopher Lim
 */
public class CourseDetail extends AppCompatActivity implements  OnItemSelectedListener{
    private EditText courseNameEditText;
    private EditText courseStartDateEditText;
    private EditText courseEndDateEditText;
    private Spinner courseStatus;
    private Spinner courseTerm;
    private EditText courseInstructorNameEditText;
    private EditText courseInstructorPhoneEditText;
    private EditText courseInstructorEmailEditText;
    private EditText courseNotesEditText;
    private Button saveButton;

    int numberAssessments;

    Repository repository;
    private int termId;
    private int courseId;
    private String status;
    private List<TermEntities> terms;
    CourseEntities currentCourse;

    // This is used for the Calendar Date Picker
    DatePickerDialog.OnDateSetListener courseStartDate;
    final Calendar courseCalendarStartDate = Calendar.getInstance();

    DatePickerDialog.OnDateSetListener courseEndDate;
    final Calendar courseCalendarEndDate = Calendar.getInstance();

    /**
     * This is the constructor for Course Detail
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_detail);

        repository = new Repository(getApplication());

        // this is for course edit text
        courseNameEditText = findViewById(R.id.courseNameEditText);
        courseStartDateEditText = findViewById(R.id.courseEditStartDate);
        courseEndDateEditText = findViewById(R.id.courseEditEndDate);

        // this is for the spinner or dropdown
        courseTerm = findViewById(R.id.termSpinner);
        courseStatus = (Spinner) findViewById(R.id.courseSpinner);

        // this is for instructor edit text
        courseInstructorNameEditText = findViewById(R.id.courseInstructorName);
        courseInstructorPhoneEditText = findViewById(R.id.courseInstructorPhone);
        courseInstructorEmailEditText = findViewById(R.id.courseInstructorEmail);

        // this is for course notes edit text
        courseNotesEditText = findViewById(R.id.courseNote);

        // this is for the course save button
        saveButton = findViewById(R.id.saveButton2);

        // This is for the date picker
        String dateFormat = "MM/dd/yy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat, Locale.US);

        // This is for the start date
        courseStartDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date date = Calendar.getInstance().getTime();
                String info = courseStartDateEditText.getText().toString();

                if (info.equals("")){
                    info = simpleDateFormat.format(date);
                }

                try {
                    courseCalendarStartDate.setTime(simpleDateFormat.parse(info));
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                new DatePickerDialog(CourseDetail.this, courseStartDate, courseCalendarStartDate
                        .get(Calendar.YEAR), courseCalendarStartDate.get(Calendar.MONTH), courseCalendarStartDate.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        courseStartDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String dateFormat = "MM/dd/yy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);

                courseCalendarStartDate.set(Calendar.YEAR, year);
                courseCalendarStartDate.set(Calendar.MONTH, month);
                courseCalendarStartDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                courseStartDateEditText.setText(sdf.format(courseCalendarStartDate.getTime()));
            }
        };

        // This is for the end date
        courseEndDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date date = Calendar.getInstance().getTime();
                String info = courseEndDateEditText.getText().toString();

                if (info.equals("")){
                    info = simpleDateFormat.format(date);
                }

                try {
                    courseCalendarEndDate.setTime(simpleDateFormat.parse(info));
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                new DatePickerDialog(CourseDetail.this, courseEndDate, courseCalendarEndDate
                        .get(Calendar.YEAR), courseCalendarEndDate.get(Calendar.MONTH), courseCalendarEndDate.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        courseEndDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String dateFormat = "MM/dd/yy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);

                courseCalendarEndDate.set(Calendar.YEAR, year);
                courseCalendarEndDate.set(Calendar.MONTH, month);
                courseCalendarEndDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                courseEndDateEditText.setText(sdf.format(courseCalendarEndDate.getTime()));
            }
        };

        // this will pass the course data into the course adapter
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            courseId = extras.getInt("courseId");
            termId = extras.getInt("termId");

            String courseTitle = extras.getString("title");
            String courseStartDate = extras.getString("start");
            String courseEndDate = extras.getString("end");
            status = extras.getString("status");

            String instructorName = extras.getString("courseInstructorName");
            String instructorPhone = extras.getString("courseInstructorEmail");
            String instructorEmail = extras.getString("courseInstructorPhoneNumber");

            String courseNotes = extras.getString("note");

            // this will fill the Edit Text with the Course Details
            courseNameEditText.setText(courseTitle);
            courseStartDateEditText.setText(courseStartDate);
            courseEndDateEditText.setText(courseEndDate);

            courseInstructorNameEditText.setText(instructorName);
            courseInstructorPhoneEditText.setText(instructorPhone);
            courseInstructorEmailEditText.setText(instructorEmail);

            courseNotesEditText.setText(courseNotes);
        }

        // this populate the term spinner
        terms = repository.getAllTerms();

        // We created an array list of all the terms
        ArrayList<TermEntities> termEntitiesArrayList = new ArrayList<>();
        termEntitiesArrayList.addAll(terms);

        // This is where we use the array list and populate the drop down for terms
        ArrayAdapter<TermEntities> spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, termEntitiesArrayList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        courseTerm.setAdapter(spinnerAdapter);

        // if the term is greater than zero, we make the selection termId - 1
        if (termId > 0) {
            courseTerm.setSelection(termId - 1);
        }

        // This is used for selecting the course term in the drop down
        courseTerm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TermEntities selectedTerm = terms.get(position);
                termId = selectedTerm.getTermId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        // this populate the course status spinner
        ArrayAdapter<CharSequence> courseStatusAdapter = ArrayAdapter.createFromResource(
                this, R.array.course_status_list, android.R.layout.simple_spinner_item);
        courseStatusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        courseStatus.setAdapter(courseStatusAdapter);

        // This is making sure that, if we pass the data from the course list
        // we make sure we put the status of the item to the detail status selection
        if (status != null) {
            if (status.equals("In Progress")) {
                courseStatus.setSelection(0);
            }
            else if (status.equals("Completed")) {
                courseStatus.setSelection(1);
            }
            else if (status.equals("Dropped")) {
                courseStatus.setSelection(2);
            } else if (status.equals("Planned to Take")){
                courseStatus.setSelection(3);
            }
        }
        // We use this listener to select the options for the status
        courseStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                status = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });



        // this is for saving a new course
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String courseTitle = courseNameEditText.getText().toString();
                String courseStartDate = courseStartDateEditText.getText().toString();
                String courseEndDate = courseEndDateEditText.getText().toString();
                String instructorName = courseInstructorNameEditText.getText().toString();
                String instructorPhone = courseInstructorPhoneEditText.getText().toString();
                String instructorEmail = courseInstructorEmailEditText.getText().toString();
                String courseNotes = courseNotesEditText.getText().toString();

                repository = new Repository(getApplication());
                CourseEntities course;

                // We check if the edit text fields are empty and if they are show the toast message
                if (courseTitle.trim().isEmpty() || courseStartDate.trim().isEmpty() || courseEndDate.trim().isEmpty()
                        || instructorName.trim().isEmpty() || instructorPhone.trim().isEmpty() || instructorEmail.trim().isEmpty() ||
                        courseNotes.trim().isEmpty()) {
                    Toast.makeText(CourseDetail.this, "The Fields are empty.", Toast.LENGTH_LONG).show();
                    return;
                }

                // if the course id is not equal to zero, we use course update instead of inserts
                if (courseId != 0) {
                    // This is for existing course
                    course = new CourseEntities(courseId, termId, courseTitle, courseStartDate, courseEndDate,
                            status, instructorName, instructorPhone, instructorEmail, courseNotes);
                    repository.update(course);
                    Toast.makeText(CourseDetail.this, courseTitle + " has been successfully updated.", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(CourseDetail.this, CourseList.class);
                    startActivity(intent);
                } else {
                    // This is for making a new term
                    course = new CourseEntities(courseId, termId, courseTitle, courseStartDate, courseEndDate,
                            status, instructorName, instructorPhone, instructorEmail, courseNotes);
                    repository.insert(course);
                    Toast.makeText(CourseDetail.this, courseTitle + " has been successfully added.", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(CourseDetail.this, CourseList.class);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    /**
     * This will create the menu options for Course detail page
     * @param menu The options menu in which you place your items.
     *
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_course_detail, menu);
        return true;
    }

    /**
     * This method is used to select the menu items.
     * @param item The menu item that was selected.
     *
     * @return true
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        repository = new Repository(getApplication());
        String courseTitle = courseNameEditText.getText().toString();
        String courseStartDate = courseStartDateEditText.getText().toString();
        String courseEndDate = courseEndDateEditText.getText().toString();
        String instructorName = courseInstructorNameEditText.getText().toString();
        String instructorPhone = courseInstructorPhoneEditText.getText().toString();
        String instructorEmail = courseInstructorEmailEditText.getText().toString();
        String courseNotes = courseNotesEditText.getText().toString();

        String dateFormat = "MM/dd/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat, Locale.US);

        // This menu item let us return to the term list
        if (item.getItemId() == R.id.course_back) {
            Intent intent = new Intent(CourseDetail.this, CourseList.class);
            startActivity(intent);
        }

        // This menu item let us delete the course
        if (item.getItemId() == R.id.course_delete) {
            if (courseTitle.trim().isEmpty() || courseStartDate.trim().isEmpty() || courseEndDate.trim().isEmpty()
                    || instructorName.trim().isEmpty() || instructorPhone.trim().isEmpty() || instructorEmail.trim().isEmpty() ||
                    courseNotes.trim().isEmpty()) {
                Toast.makeText(CourseDetail.this, "The Fields are empty.", Toast.LENGTH_LONG).show();
                return true;
            } else {

                // This will iterate in all courses and find the current course
                for (CourseEntities course : repository.getAllCourses()) {
                    if (course.getCourseId() == courseId) {
                        currentCourse = course;
                    } else if (course.getCourseId() == 0) {
                        Toast.makeText(CourseDetail.this, "Cannot Delete a Course that has no courseId.", Toast.LENGTH_LONG).show();
                        return true;
                    }
                }

                // We set the field number assessments to 0
                numberAssessments = 0;

                for (AssessmentEntities assessment : repository.getAllAssessments()) {
                    if (assessment.getCourseId() == courseId) {
                        ++numberAssessments;
                    }
                }


                // If there is no assessment attach to a course, we let the user delete the course
                if (numberAssessments == 0) {
                    repository.delete(currentCourse);
                    Toast.makeText(CourseDetail.this, currentCourse.getCourseTitle() + " has been successfully deleted.", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(CourseDetail.this, TermList.class);
                    startActivity(intent);
                }
                // And if we found at lease one assessment assigned to the course, we do not let the user delete the term
                else {
                    Toast.makeText(CourseDetail.this, currentCourse.getCourseTitle() + " has one or more assessment and cannot be deleted.", Toast.LENGTH_LONG).show();
                }
            }
        }

        // This menu item let us view all the assessment
        if (item.getItemId() == R.id.course_assessments) {
            Intent intent = new Intent(CourseDetail.this, AssessmentList.class);
            startActivity(intent);
        }

        // This menu will share notes
        if (item.getItemId() == R.id.action_share) {
            try {
                Intent notesIntent = new Intent(Intent.ACTION_SEND);
                notesIntent.setType("text/plain");
                notesIntent.putExtra(Intent.EXTRA_TITLE, courseNotes);
                notesIntent.putExtra(Intent.EXTRA_TITLE, "Notes from " + courseTitle);
                startActivity(Intent.createChooser(notesIntent, "Share Via"));
                Toast.makeText(CourseDetail.this, "Course Notes has been successfully shared.", Toast.LENGTH_LONG).show();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // This menu will send alerts for the course start date
        if (item.getItemId() == R.id.action_notify_start) {
            try {
                Date startDate = simpleDateFormat.parse(courseStartDate);

                Long startDateTrigger = startDate.getTime();
                Intent startDateIntent = new Intent(CourseDetail.this, MyReceiver.class);
                startDateIntent.putExtra("key", courseTitle + " has started!");

                PendingIntent sender = PendingIntent.getBroadcast(CourseDetail.this, Home.numAlert++, startDateIntent,
                        PendingIntent.FLAG_IMMUTABLE);

                AlarmManager startDateAlarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                startDateAlarmManager.set(AlarmManager.RTC_WAKEUP, startDateTrigger, sender);

                return true;
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }

        // This menu will send alerts for the course end date
        if (item.getItemId() == R.id.action_notify_end) {
            try {
                Date endDate = simpleDateFormat.parse(courseEndDate);

                Long endDateTrigger = endDate.getTime();
                Intent endDateIntent = new Intent(CourseDetail.this, MyReceiver.class);
                endDateIntent.putExtra("key", courseTitle + " has ended!");

                PendingIntent sender = PendingIntent.getBroadcast(CourseDetail.this, Home.numAlert++, endDateIntent,
                        PendingIntent.FLAG_IMMUTABLE);

                AlarmManager endDateAlarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                endDateAlarmManager.set(AlarmManager.RTC_WAKEUP, endDateTrigger, sender);

                return true;
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }

        return true;
    }
}