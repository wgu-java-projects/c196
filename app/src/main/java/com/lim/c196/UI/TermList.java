package com.lim.c196.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import com.lim.c196.Adapter.TermAdapter;
import com.lim.c196.Database.Repository;
import com.lim.c196.Entities.TermEntities;
import com.lim.c196.R;

import java.util.List;

/**
 * A class for Term List Activity
 * @author Christopher Lim
 */
public class TermList extends AppCompatActivity implements TermAdapter.OnItemClickListener{
    private TermAdapter adapter;

    /**
     * This method creates the TermList page
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_list);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        Repository repository = new Repository(getApplication());
        List<TermEntities> terms = repository.getAllTerms();
        adapter = new TermAdapter(this, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter.setTerms(terms);

        // This is the Plus button and used to add new term
        FloatingActionButton floatingActionButton = findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            // When onClick method is triggered we move from term list page to term detail page
            // and then we can full up the form
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TermList.this, TermDetail.class);
                startActivity(intent);
            }
        });
    }

    /**
     * This method is used when the app resumes its activity in the TermList.
     * And you can think of this method as an automatic refresh.
     */
    @Override
    protected void onResume() {
        super.onResume();
        Repository repository = new Repository(getApplication());
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        final TermAdapter termAdapter = new TermAdapter(this,this);

        recyclerView.setAdapter(termAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        List<TermEntities> termEntities = repository.getAllTerms();
        termAdapter.setTerms(termEntities);
    }

    /**
     * This method is used to transfer the selected term data into the
     * term add edit page.
     * @param term
     */
    @Override
    public void onItemClick(TermEntities term) {
        Intent intent = new Intent(TermList.this, TermDetail.class);
        intent.putExtra("termId", term.getTermId());
        intent.putExtra("title", term.getTermTitle());
        intent.putExtra("start", term.getTermStartDate());
        intent.putExtra("end", term.getTermEndDate());
        startActivity(intent);
    }
}