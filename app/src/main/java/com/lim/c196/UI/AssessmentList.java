package com.lim.c196.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.lim.c196.Adapter.AssessmentAdapter;
import com.lim.c196.Database.Repository;
import com.lim.c196.Entities.AssessmentEntities;
import com.lim.c196.R;

import java.util.List;

/**
 * A class for Assessment List Activity
 * @author Christopher Lim
 */
public class AssessmentList extends AppCompatActivity implements AssessmentAdapter.OnItemClickListener{
    private AssessmentAdapter adapter;

    /**
     * This method creates the AssessmentList page
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assessment_list);

        RecyclerView recyclerView = findViewById(R.id.recyclerView3);
        Repository repository = new Repository(getApplication());
        List<AssessmentEntities> assessments = repository.getAllAssessments();
        adapter = new AssessmentAdapter(this, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter.setAssessments(assessments);

        // This is the Plus button and used to add new assessment
        FloatingActionButton floatingActionButton = findViewById(R.id.floatingActionButton3);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AssessmentList.this, AssessmentDetail.class);
                startActivity(intent);
            }
        });
    }

    // This method is used for refreshing the page or screen.
    // When there is an update, like creating a new record, updating a record or deleting a record.
    // We need to make sure the page is up to date
    @Override
    protected void onResume() {
        super.onResume();
        Repository repository = new Repository(getApplication());
        RecyclerView recyclerView = findViewById(R.id.recyclerView3);
        final AssessmentAdapter assessmentAdapter = new AssessmentAdapter(this,this);

        recyclerView.setAdapter(assessmentAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        List<AssessmentEntities> assessmentEntities = repository.getAllAssessments();
        assessmentAdapter.setAssessments(assessmentEntities);
    }

    // We use this method to pass the needed data to populate the
    // assessment detail page
    @Override
    public void onItemClick(AssessmentEntities assessment) {
        Intent intent = new Intent(AssessmentList.this, AssessmentDetail.class);
        intent.putExtra("assessmentId", assessment.getAssessmentId());
        intent.putExtra("courseId", assessment.getCourseId());
        intent.putExtra("title", assessment.getAssessmentTitle());
        intent.putExtra("start", assessment.getAssessmentStartDate());
        intent.putExtra("end", assessment.getAssessmentEndDate());
        intent.putExtra("type", assessment.getAssessmentType());
        intent.putExtra("content", assessment.getAssessmentContent());
        startActivity(intent);
    }
}