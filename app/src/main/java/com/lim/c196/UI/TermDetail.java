package com.lim.c196.UI;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.lim.c196.Database.Repository;
import com.lim.c196.Entities.CourseEntities;
import com.lim.c196.Entities.TermEntities;
import com.lim.c196.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * A class for Term Detail Activity
 * @author Christopher Lim
 */
public class TermDetail extends AppCompatActivity {
    private EditText termTitleEditText;
    private EditText startDateEditText;
    private EditText endDateEditText;
    private Button saveButton;

    int numberCourses;
    private int termId;
    Repository repository;
    TermEntities currentTerm;

    // This is used for the Calendar Date Picker
    DatePickerDialog.OnDateSetListener termStartDate;
    final Calendar termCalendarStartDate = Calendar.getInstance();

    DatePickerDialog.OnDateSetListener termEndDate;
    final Calendar termCalendarEndDate = Calendar.getInstance();

    /**
     * This is the constructor for Term Detail
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_detail);

        // This is where we attach the layout ids to the fields
        termTitleEditText = findViewById(R.id.TermTitletext);
        startDateEditText = findViewById(R.id.editStartDateterm);
        endDateEditText = findViewById(R.id.editEndDateterm);
        saveButton = findViewById(R.id.saveButton);


        // This is for the date picker
        String dateFormat = "MM/dd/yy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat, Locale.US);

        // This is for the start date
        startDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date date = Calendar.getInstance().getTime();
                String info = startDateEditText.getText().toString();

                if (info.equals("")){
                    info = simpleDateFormat.format(date);
                }

                try {
                    termCalendarStartDate.setTime(simpleDateFormat.parse(info));
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                new DatePickerDialog(TermDetail.this, termStartDate, termCalendarStartDate
                        .get(Calendar.YEAR), termCalendarStartDate.get(Calendar.MONTH), termCalendarStartDate.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        termStartDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String dateFormat = "MM/dd/yy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);

                termCalendarStartDate.set(Calendar.YEAR, year);
                termCalendarStartDate.set(Calendar.MONTH, month);
                termCalendarStartDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                startDateEditText.setText(sdf.format(termCalendarStartDate.getTime()));
            }
        };

        // This is for the end date
        endDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date date = Calendar.getInstance().getTime();
                String info = endDateEditText.getText().toString();

                if (info.equals("")){
                    info = simpleDateFormat.format(date);
                }

                try {
                    termCalendarEndDate.setTime(simpleDateFormat.parse(info));
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                new DatePickerDialog(TermDetail.this, termEndDate, termCalendarEndDate
                        .get(Calendar.YEAR), termCalendarEndDate.get(Calendar.MONTH), termCalendarEndDate.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        termEndDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String dateFormat = "MM/dd/yy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);

                termCalendarEndDate.set(Calendar.YEAR, year);
                termCalendarEndDate.set(Calendar.MONTH, month);
                termCalendarEndDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                endDateEditText.setText(sdf.format(termCalendarEndDate.getTime()));
            }
        };


        // This will get the term data and passes it to the TermAdapter
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            termId = extras.getInt("termId");
            String title = extras.getString("title");
            String start = extras.getString("start");
            String end = extras.getString("end");

            // This will fill the Edit Text with the Term Details
            termTitleEditText.setText(title);
            startDateEditText.setText(start);
            endDateEditText.setText(end);
        }

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String termTitle = termTitleEditText.getText().toString();
                String termStartDate = startDateEditText.getText().toString();
                String termEndDate = endDateEditText.getText().toString();

                repository = new Repository(getApplication());
                TermEntities term;

                // Validation check to make sure the fields are not empty
                if (termTitle.trim().isEmpty() || termStartDate.trim().isEmpty() || termEndDate.trim().isEmpty()) {
                    Toast.makeText(TermDetail.this, "The Fields are empty.", Toast.LENGTH_LONG).show();
                    return;
                }

                if (termId != 0) {
                    // This is for existing term
                    term = new TermEntities(termId, termTitle, termStartDate, termEndDate);
                    repository.update(term);
                    Toast.makeText(TermDetail.this, termTitle + " has been successfully updated.", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(TermDetail.this, TermList.class);
                    startActivity(intent);
                } else {
                    // This is for making a new term
                    term = new TermEntities(termId, termTitle, termStartDate, termEndDate);
                    repository.insert(term);
                    Toast.makeText(TermDetail.this, termTitle + " has been successfully added.", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(TermDetail.this, TermList.class);
                    startActivity(intent);
                }
            }
        });
    }

    /**
     * This method will create the menu
     * @param menu The options menu in which you place your items.
     *
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_term_detail, menu);
        return true;
    }

    /**
     * This method is used to select the menu items.
     * @param item The menu item that was selected.
     *
     * @return true
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        repository = new Repository(getApplication());
        String termTitle = termTitleEditText.getText().toString();
        String termStartDate = startDateEditText.getText().toString();
        String termEndDate = endDateEditText.getText().toString();

        // This will call the delete term menu item
        if (item.getItemId() == R.id.term_delete) {
            if (termTitle.trim().isEmpty() || termStartDate.trim().isEmpty() || termEndDate.trim().isEmpty()) {
                Toast.makeText(TermDetail.this, "The Fields are empty.", Toast.LENGTH_LONG).show();
                return true;
            } else {

                // This will iterate in all the terms and find the current term
                for (TermEntities term : repository.getAllTerms()) {
                    if (term.getTermId() == termId) {
                        currentTerm = term;
                    } else if (term.getTermId() == 0){
                        Toast.makeText(TermDetail.this, "Cannot Delete a Term that has no termId.", Toast.LENGTH_LONG).show();
                        return true;
                    }
                }

                // We set the field number courses to 0
                numberCourses = 0;

                // After we found the current term, we will do a for each
                // to find how many courses in the term and add it to the
                // numberCourses field
                for (CourseEntities course : repository.getAllCourses()) {
                    if (course.getTermId() == termId) {
                        ++numberCourses;
                    }
                }

                // If there is no courses attach to a term, we let the user delete the term
                if (numberCourses == 0) {
                    repository.delete(currentTerm);
                    Toast.makeText(TermDetail.this, currentTerm.getTermTitle() + " has been successfully deleted.", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(TermDetail.this, TermList.class);
                    startActivity(intent);
                }
                // And if we found at lease one course assigned to the term, we do not let the user delete the term
                else {
                    Toast.makeText(TermDetail.this, currentTerm.getTermTitle() + " has one or more courses and cannot be deleted.", Toast.LENGTH_LONG).show();
                }
            }
        }

        // This menu item let us view all the courses that is attached to the current term
        if (item.getItemId() == R.id.term_course) {
            Intent intent = new Intent(TermDetail.this, CourseList.class);
            startActivity(intent);
        }

        // This menu item let us return to the term list
        if (item.getItemId() == R.id.term_back) {
            Intent intent = new Intent(TermDetail.this, TermList.class);
            startActivity(intent);
        }

        return true;
    }
}
