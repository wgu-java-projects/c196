package com.lim.c196.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.lim.c196.Adapter.CourseAdapter;
import com.lim.c196.Database.Repository;
import com.lim.c196.Entities.CourseEntities;
import com.lim.c196.R;

import java.util.List;

/**
 * A class for Course List Activity
 * @author Christopher Lim
 */
public class CourseList extends AppCompatActivity implements CourseAdapter.OnItemClickListener{
    private CourseAdapter adapter;

    /**
     * This method creates the CourseList page
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_list);

        RecyclerView recyclerView = findViewById(R.id.recyclerView2);
        Repository repository = new Repository(getApplication());
        List<CourseEntities> courses = repository.getAllCourses();
        adapter = new CourseAdapter(this, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter.setCourses(courses);

        // This is the Plus button and used to add new course
        FloatingActionButton floatingActionButton = findViewById(R.id.floatingActionButton2);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CourseList.this, CourseDetail.class);
                startActivity(intent);
            }
        });
    }

    /**
     * This method is used when the app resumes its activity in the CourseList.
     * And you can think of this method as an automatic refresh.
     */
    @Override
    protected void onResume() {
        super.onResume();
        Repository repository = new Repository(getApplication());
        RecyclerView recyclerView = findViewById(R.id.recyclerView2);
        final CourseAdapter courseAdapter = new CourseAdapter(this,this);

        recyclerView.setAdapter(courseAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        List<CourseEntities> courseEntities = repository.getAllCourses();
        courseAdapter.setCourses(courseEntities);
    }

    /**
     * This method is used to transfer the selected course data into the
     * course add edit page.
     * @param course
     */
    @Override
    public void onItemClick(CourseEntities course) {
        Intent intent = new Intent(CourseList.this, CourseDetail.class);
        intent.putExtra("termId", course.getTermId());
        intent.putExtra("courseId", course.getCourseId());
        intent.putExtra("title", course.getCourseTitle());
        intent.putExtra("start", course.getCourseStartDate());
        intent.putExtra("end", course.getCourseEndDate());
        intent.putExtra("status", course.getCourseStatus());
        intent.putExtra("courseInstructorName", course.getCourseInstructorName());
        intent.putExtra("courseInstructorEmail", course.getCourseInstructorEmail());
        intent.putExtra("courseInstructorPhoneNumber", course.getCourseInstructorPhoneNumber());
        intent.putExtra("note", course.getCourseNotes());
        startActivity(intent);
    }
}