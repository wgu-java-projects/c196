package com.lim.c196.UI;

import android.app.Notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.lim.c196.R;

/**
 * A class for MyReceiver to Broadcast Notification
 * @author Christopher Lim
 */
public class MyReceiver extends BroadcastReceiver {
    String channel_id = "test";
    static int notificationId;

    /**
     * This method is used to receive the notification
     * @param context
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        // This will show a toast message with the key
        Toast.makeText(context, intent.getStringExtra("key"), Toast.LENGTH_LONG).show();

        // We are assigning the key to the String variable message
        String message = intent.getStringExtra("key");

        // Now we are creating a notification channel with a context and channel_id
        createNotificationChannel(context, channel_id);

        // This is the notification builder, in which we assign an icon, content text and content title
        Notification notification = new NotificationCompat.Builder(context, channel_id)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentText(message)
                .setContentTitle("Notification").build();

        // This is where we use the notification manager to manage our notification service
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(notificationId, notification);

    }

    /**
     * This method creates the notification channel
     * @param context
     * @param channel_id
     */
    private void createNotificationChannel(Context context, String channel_id){
        // These are the fields for the channel name, channel description and an integer
        // for the importaqnce default
        CharSequence name="myChannelName";
        String description="myChannelDescription";
        int importance= NotificationManager.IMPORTANCE_DEFAULT;

        // This is where we are creating a new notification channel and we are passing
        // the channel id, name and importance
        NotificationChannel nc = new NotificationChannel(channel_id,name,importance);
        nc.setDescription(description);

        // This is where we use the notification manager to create a notification channel
        NotificationManager notificationManager=context.getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(nc);
    }
}
