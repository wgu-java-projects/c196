package com.lim.c196.Database;

import android.app.Application;

import com.lim.c196.DAO.AssessmentDAO;
import com.lim.c196.DAO.CourseDAO;
import com.lim.c196.DAO.TermDAO;
import com.lim.c196.Entities.AssessmentEntities;
import com.lim.c196.Entities.CourseEntities;
import com.lim.c196.Entities.TermEntities;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * A class that acts as a bridge between data sources
 * and rest of the app.
 * @author Christopher Lim
 */
public class Repository {
    private TermDAO mTermDAO;
    private CourseDAO mCourseDAO;
    private AssessmentDAO mAssessmentDAO;

    private List<TermEntities> mAllTerms;
    private List<CourseEntities> mAllCourses;
    private List<AssessmentEntities> mAllAssessments;

    private static int NUMBER_OF_THREADS = 5;
    static final ExecutorService databaseExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    /**
     * This is the constructor od the Repository
     * @param application
     */
    public Repository(Application application) {
        DatabaseBuilder databaseBuilder = DatabaseBuilder.getDatabase(application);
        mTermDAO = databaseBuilder.termDAO();
        mCourseDAO = databaseBuilder.courseDAO();
        mAssessmentDAO = databaseBuilder.assessmentDAO();
    }

    /**
     * This method is for getting all terms
     * @return mAllTerms
     */
    public List<TermEntities>getAllTerms(){
        databaseExecutor.execute(()->{
            mAllTerms=mTermDAO.getAllTerms();
        });

        // we made the system wait 1 second before it process
        // another request and it will print a stack trace if
        // something went wrong
        try{
            Thread.sleep(1000);
        } catch (InterruptedException e ){
            e.printStackTrace();
        }
        return mAllTerms;
    }

    /**
     * This method is used to insert new terms
     * @param term
     */
    public void insert(TermEntities term){
        databaseExecutor.execute(() ->{
            mTermDAO.insert(term);

        });

        // we made the system wait 1 second before it process
        // another request and it will print a stack trace if
        // something went wrong
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is used for updating the existing term
     * @param term
     */
    public void update(TermEntities term) {
        databaseExecutor.execute(() ->{
            mTermDAO.update(term);
        });

        // we made the system wait 1 second before it process
        // another request and it will print a stack trace if
        // something went wrong
        try{
            Thread.sleep(1000);
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    /**
     * This method is for deleting the existing term
     * @param term
     */
    public void delete(TermEntities term){
        databaseExecutor.execute(() ->{
            mTermDAO.delete(term);
        });

        // we made the system wait 1 second before it process
        // another request and it will print a stack trace if
        // something went wrong
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    /**
     * This method is used to get all courses
     * @return mAllCourses
     */
    public List<CourseEntities>getAllCourses(){
        databaseExecutor.execute(()->{
            mAllCourses=mCourseDAO.getAllCourses();

        });

        // we made the system wait 1 second before it process
        // another request and it will print a stack trace if
        // something went wrong
        try{
            Thread.sleep(1000);
        } catch (InterruptedException e ){
            e.printStackTrace();
        }
        return mAllCourses;
    }

    /**
     * This method is used to insert new course
     * @param course
     */
    public void insert(CourseEntities course){
        databaseExecutor.execute(() ->{
            mCourseDAO.insert(course);

        });

        // we made the system wait 1 second before it process
        // another request and it will print a stack trace if
        // something went wrong
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is used for updating the existing course
     * @param course
     */
    public void update(CourseEntities course) {
        databaseExecutor.execute(() ->{
            mCourseDAO.update(course);
        });

        // we made the system wait 1 second before it process
        // another request and it will print a stack trace if
        // something went wrong
        try{
            Thread.sleep(1000);
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    /**
     * This method will delete an existing course
     * @param course
     */
    public void delete(CourseEntities course){
        databaseExecutor.execute(() ->{
            mCourseDAO.delete(course);
        });

        // we made the system wait 1 second before it process
        // another request and it will print a stack trace if
        // something went wrong
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    /**
     * This method is for getting all the assessment
     * @return mAllAssessments
     */
    public List<AssessmentEntities>getAllAssessments(){
        databaseExecutor.execute(()->{
            mAllAssessments= mAssessmentDAO.getAllAssessments();
        });

        // we made the system wait 1 second before it process
        // another request and it will print a stack trace if
        // something went wrong
        try{
            Thread.sleep(1000);
        } catch (InterruptedException e ){
            e.printStackTrace();
        }
        return mAllAssessments;
    }

    /**
     * This method is for inserting new assessment
     * @param assessment
     */
    public void insert(AssessmentEntities assessment){
        databaseExecutor.execute(() ->{
            mAssessmentDAO.insert(assessment);
        });

        // we made the system wait 1 second before it process
        // another request and it will print a stack trace if
        // something went wrong
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is for updating existing assessment
     * @param assessment
     */
    public void update(AssessmentEntities assessment) {
        databaseExecutor.execute(() ->{
            mAssessmentDAO.update(assessment);
        });

        // we made the system wait 1 second before it process
        // another request and it will print a stack trace if
        // something went wrong
        try{
            Thread.sleep(1000);
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    /**
     * This method is for deleting existing assessment
     * @param assessment
     */
    public void delete(AssessmentEntities assessment){
        databaseExecutor.execute(() ->{
            mAssessmentDAO.delete(assessment);
        });

        // we made the system wait 1 second before it process
        // another request and it will print a stack trace if
        // something went wrong
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
