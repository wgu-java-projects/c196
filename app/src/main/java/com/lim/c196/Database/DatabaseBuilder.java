package com.lim.c196.Database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.lim.c196.DAO.AssessmentDAO;
import com.lim.c196.DAO.CourseDAO;
import com.lim.c196.DAO.TermDAO;
import com.lim.c196.Entities.AssessmentEntities;
import com.lim.c196.Entities.CourseEntities;
import com.lim.c196.Entities.TermEntities;

/**
 * An abstract class to Build the Database
 * @author Christopher Lim
 */
@Database(entities = { AssessmentEntities.class, CourseEntities.class, TermEntities.class}, version = 8, exportSchema = false)
public abstract class DatabaseBuilder extends RoomDatabase {
    public abstract TermDAO termDAO();
    public abstract CourseDAO courseDAO();
    public abstract AssessmentDAO assessmentDAO();

    private static volatile DatabaseBuilder INSTANCE;

    /**
     * This method builds the database
     * @param context
     * @return INSTANCE
     */
    static DatabaseBuilder getDatabase(final Context context) {
        // if we the instance of the database is null we synchronized it
        if (INSTANCE==null){
            synchronized (DatabaseBuilder.class){
                if(INSTANCE==null){
                    INSTANCE= Room.databaseBuilder(context.getApplicationContext(),DatabaseBuilder.class, "TermDatabase.db")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
