package com.lim.c196.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.lim.c196.Entities.CourseEntities;

import java.util.List;

/**
 * An interface for CourseDao that will have inserts
 *  * update, delete and query all courses
 * @author Christopher Lim
 */
@Dao
public interface CourseDAO {
    /**
     * This method will insert the course
     * @param courseEntities
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(CourseEntities courseEntities);

    /**
     * This method will update the course
     * @param courseEntities
     */
    @Update
    void update(CourseEntities courseEntities);

    /**
     * This method will delete the course
     * @param courseEntities
     */
    @Delete
    void delete(CourseEntities courseEntities);

    /**
     * This method will query all courses
     * @return getAllCourses
     */
    @Query("SELECT * FROM Course ORDER BY courseId ASC")
    List<CourseEntities> getAllCourses();
}
