package com.lim.c196.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.lim.c196.Entities.AssessmentEntities;

import java.util.List;

/**
 * An interface for AssessmentDao that will have inserts
 *  * update, delete and query all assessment
 * @author Christopher Lim
 */
@Dao
public interface AssessmentDAO {
    /**
     * This method inserts the assessment
     * @param assessmentEntities
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert (AssessmentEntities assessmentEntities);

    /**
     * This method update the assessment
     * @param assessmentEntities
     */
    @Update
    void update (AssessmentEntities assessmentEntities);

    /**
     * This method deletes the assessment
     * @param assessmentEntities
     */
    @Delete
    void delete (AssessmentEntities assessmentEntities);

    /**
     * This method will query all the assessment
     * @return getAllAssessment
     */
    @Query("SELECT * FROM Assessment ORDER BY courseId ASC")
    List<AssessmentEntities> getAllAssessments();
}
