package com.lim.c196.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.lim.c196.Entities.TermEntities;

import java.util.List;

/**
 * An interface for TermDao that will have inserts
 * update, delete and query all terms
 * @author Christopher Lim
 */
@Dao
public interface TermDAO {
    /**
     * This method will insert a term
     * @param termEntities
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(TermEntities termEntities);

    /**
     * This method will update a term
     * @param termEntities
     */
    @Update
    void update(TermEntities termEntities);

    /**
     * This method will delete a term
     * @param termEntities
     */
    @Delete
    void delete(TermEntities termEntities);

    /**
     * This method will query all terms
     * @return getAllTerms
     */
    @Query("SELECT * FROM Term ORDER BY termId ASC")
    List<TermEntities> getAllTerms();
}
