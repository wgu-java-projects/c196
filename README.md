# Term Tracker App

Students can use this smartphone app to track terms, courses, and assessments for each course.
Students can add, edit, and delete term, course, and assessment data via the app.
Provide summary and extensive course views for each term, including alerts for future performance
and objective assessments. This application utilizes SQLite database.

### Documents TO DO List
- [X] StoryBoard - Need to take screenshot of the app
- [X] Screenshot of APK being generated
- [X] Reflect on the creation of your mobile application by doing the following:
1. Explain how your application would be different if it were developed for a tablet rather than a phone,
   including a discussion of fragments and layouts.
2. Identify the minimum and target operating system your application was developed under and is
   compatible with.
3. Describe ( suggested length of 1–2 paragraphs) the challenges you faced during the development of the
   mobile application.
4. Describe ( suggested length of 1–2 paragraphs) how you overcame each challenge discussed in part F3.
5. Discuss ( suggested length of 1–2 paragraphs) what you would do differently if you did the project
   again.
6. Describe how emulators are used and the pros and cons of using an emulator versus using a
   development device.

### Screenshots
![Home](Documents/images/1-home.png)
![Term List](Documents/images/1-home.png)
![Term Detail](Documents/images/3-term-detail.png)
![Course List](Documents/images/4-course-list.png)
![Course Detail](Documents/images/5-course-detail.png)
![Assessment List](Documents/images/6-assessment-list.png)
![Assessment Detail](Documents/images/7-assessment-detail.png)